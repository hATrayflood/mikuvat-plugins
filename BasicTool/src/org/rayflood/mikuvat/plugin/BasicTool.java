package org.rayflood.mikuvat.plugin;

import static org.rayflood.mikuvat.Mikuvat.printTextResource;

import java.util.Properties;

import javax.swing.JDialog;

import org.rayflood.mikuvat.gui.MikuvatFrame;
import org.rayflood.mikuvat.io.vsq.VSQFile;

public class BasicTool implements PluginInterface{
	private VSQFile vsq;
	private JDialog dialog;

	public String getPluginID() {
		return "BasicTool";
	}

	public String getPluginName() {
		return "VSQパラメータ一括平坦化ツール";
	}

	public String getPluginDescription() {
		return "VSQパラメータ一括平坦化ツール";
	}

	public void printHelp(){
		printTextResource("org/rayflood/mikuvat/plugin/BasicTool.txt");
	}

	public void start(String args[], Properties properties) {
		System.out.println(getPluginName());
	}

	public void start(MikuvatFrame frame, Properties properties) {
		vsq = frame.getVsq();

		dialog = new BasicToolDialog(frame, getPluginName());
		dialog.setVisible(true);
	}
}

package org.rayflood.mikuvat.plugin;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Window;

import org.rayflood.mikuvat.gui.AnotePanel;
import org.rayflood.mikuvat.gui.BPLinePanel;
import org.rayflood.mikuvat.gui.GridPanel;
import org.rayflood.mikuvat.gui.MasterPanel;
import org.rayflood.mikuvat.gui.PropertyDialog;
import org.rayflood.mikuvat.gui.ResoBPLinePanel;
import org.rayflood.mikuvat.gui.TrackPanel;
import org.rayflood.mikuvat.gui.TrackSelectPanel;
import org.rayflood.mikuvat.gui.VibratoHandleLabel;
import org.rayflood.mikuvat.gui.VibratoPanel;

public class BasicToolDialog extends PropertyDialog{
	private static final long serialVersionUID = -9219214496293180825L;
	private TrackSelectPanel trackselect;
	private MasterPanel master;
	private TrackPanel track;
	private AnotePanel anote;
	private VibratoPanel vibrato;
	private BPLinePanel bpline;
	private ResoBPLinePanel resobpline;

	public BasicToolDialog(Window owner, String title){
		super(owner, title);
		init();
	}

	protected void init(){
		trackselect = new TrackSelectPanel();
		trackselect.setTitledBorder();
		master = new MasterPanel();
		master.setTitledBorder();
		track = new TrackPanel();
		track.setTitledBorder();
		anote = new AnotePanel();
		anote.setTitledBorder();
		vibrato = ((VibratoHandleLabel)anote.getVibratoHandleInput()).getVibratoPanel();
		vibrato.setTitledBorder();
		bpline = new BPLinePanel();
		bpline.setTitledBorder();
		resobpline = new ResoBPLinePanel();
		resobpline.setTitledBorder();

		Component components[] = new Component[]{
				new GridPanel(new Component[]{trackselect, anote, vibrato}, 1)
				, new GridPanel(new Component[]{master, track, bpline, resobpline}, 1)
				};
		add(new GridPanel(components, 2), BorderLayout.CENTER);
		pack();
	}

	public void setSelected(boolean selected){
		trackselect.setSelected(selected);
		master.setSelected(selected);
		track.setSelected(selected);
		anote.setSelected(selected);
		vibrato.setSelected(selected);
		bpline.setSelected(selected);
		resobpline.setSelected(selected);
	}
	
}

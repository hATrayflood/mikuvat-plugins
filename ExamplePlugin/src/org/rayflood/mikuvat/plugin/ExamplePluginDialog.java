package org.rayflood.mikuvat.plugin;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

import org.rayflood.mikuvat.gui.AnoteDialog;
import org.rayflood.mikuvat.gui.AnoteVibratoDialog;
import org.rayflood.mikuvat.gui.BPLineDialog;
import org.rayflood.mikuvat.gui.BPLineResoBPDialog;
import org.rayflood.mikuvat.gui.GridPanel;
import org.rayflood.mikuvat.gui.MasterDialog;
import org.rayflood.mikuvat.gui.PropertyDialog;
import org.rayflood.mikuvat.gui.ResoBPLineDialog;
import org.rayflood.mikuvat.gui.TrackDialog;
import org.rayflood.mikuvat.gui.TrackSelectDialog;
import org.rayflood.mikuvat.gui.VibratoDialog;

public class ExamplePluginDialog extends PropertyDialog{
	private static final long serialVersionUID = -1795892808785397048L;
	private ExamplePluginDialog dialog;
	private JTextArea vsqtext;

	public ExamplePluginDialog(Window owner, String title){
		super(owner, title);
		init();
	}

	protected void init(){
		dialog = this;
		add(new JLabel("VSQデータ"), BorderLayout.NORTH);
		vsqtext = new JTextArea();
		vsqtext.setFont(Font.getFont(Font.MONOSPACED));
		vsqtext.setEditable(false);
		JScrollPane scroll = new JScrollPane(vsqtext);
		add(scroll, BorderLayout.CENTER);

		JButton b1 = new JButton("トラック選択");
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				TrackSelectDialog trackselect = new TrackSelectDialog(dialog);
				trackselect.showDialog();
			}
		});
		JButton b2 = new JButton("マスター");
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				MasterDialog master = new MasterDialog(dialog);
				master.showDialog();
			}
		});
		JButton b3 = new JButton("トラック");
		b3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				TrackDialog track = new TrackDialog(dialog);
				track.showDialog();
			}
		});
		JButton b4 = new JButton("音符");
		b4.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				AnoteDialog anote = new AnoteDialog(dialog);
				anote.showDialog();
			}
		});
		JButton b5 = new JButton("ビブラート");
		b5.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				VibratoDialog vibrato = new VibratoDialog(dialog);
				vibrato.showDialog();
			}
		});
		JButton b6 = new JButton("曲線");
		b6.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				BPLineDialog bpline = new BPLineDialog(dialog);
				bpline.showDialog();
			}
		});
		JButton b7 = new JButton("レゾナンス");
		b7.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				ResoBPLineDialog resobpline = new ResoBPLineDialog(dialog);
				resobpline.showDialog();
			}
		});
		JButton b8 = new JButton("音符・ビブラート");
		b8.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				AnoteVibratoDialog anotevibrato = new AnoteVibratoDialog(dialog);
				anotevibrato.showDialog();
			}
		});
		JButton b9 = new JButton("曲線・レゾナンス");
		b9.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				BPLineResoBPDialog bpreso = new BPLineResoBPDialog(dialog);
				bpreso.showDialog();
			}
		});
		JPanel buttons = new GridPanel(new Component[]{b1, b2, b3, b4, b5, b6, b7, b8, b9}, 1);
		buttons.setBorder(new TitledBorder("ダイアログ"));
		add(buttons, BorderLayout.EAST);
		setSize(512, 384);
	}

	public void setSelected(boolean selected){
	}

	public String getVSQText(){
		return vsqtext.getText();
	}

	public void setVSQText(String s){
		vsqtext.append(s);
		vsqtext.setCaretPosition(0);
	}
}

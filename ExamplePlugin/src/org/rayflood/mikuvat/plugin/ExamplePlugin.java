package org.rayflood.mikuvat.plugin;

import static org.rayflood.mikuvat.Mikuvat.printTextResource;

import java.util.List;
import java.util.Properties;

import org.rayflood.mikuvat.gui.MikuvatFrame;
import org.rayflood.mikuvat.io.vsq.VSQFile;

public class ExamplePlugin implements PluginInterface{
	private VSQFile vsq;

	public String getPluginDescription(){
		return "テストのプラグインだよ";
	}

	public String getPluginID(){
		return "Example";
	}

	public String getPluginName(){
		return "ExamplePlugin";
	}

	public void printHelp(){
		printTextResource("org/rayflood/mikuvat/plugin/Example.txt");
	}

	public void start(String args[], Properties properties){
		System.out.println(getPluginDescription());
		try{
			vsq = new VSQFile(args[1]);
			System.out.println(getVSQText());
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void start(MikuvatFrame frame, Properties properties){
		vsq = frame.getVsq();
		ExamplePluginDialog dialog = new ExamplePluginDialog(frame, getPluginDescription());
		dialog.setVSQText(getVSQText());
		dialog.setVisible(true);
	}

	public String getVSQText(){
		StringBuffer sb = new StringBuffer();
		try{
			List<List<String>> vsqtext = vsq.getVSQText();
//			List<List<String>> vsqtext = ((VSQFile)new VSQFile().cover(vsq)).getVSQText();
//			List<List<String>> vsqtext = ((VSQFile)VSQFile.getDefault().heap(vsq)).getVSQText();
//			List<List<String>> vsqtext = ((VSQFile)new VSQFile().lay(vsq)).getVSQText();
			vsq.writeVSQFile("test.vsq");
			for(int i = 0; i < vsqtext.size(); i++){
				List<String> section = vsqtext.get(i);
				for(int j = 0; j < section.size(); j++){
					sb.append(section.get(j) + "\n");
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
//		return vsq.toString();
	}
}
